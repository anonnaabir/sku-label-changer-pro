<?php
        

        require_once plugin_dir_path( __FILE__ ) .'/inc/csf/codestar-framework.php';

        
        // Control core classes for avoid errors
        if( class_exists( 'CSF' ) ) {

            //
            // Set a unique slug-like ID
            $prefix = 'skulabel_pro_admin';
        
            //
            // Create options
            CSF::createOptions( $prefix, array(
            'framework_title' => 'SKU Label Changer Pro',
            'menu_title' => 'SKU Label Changer Pro',
            'menu_slug'  => 'skulabel-pro',
            'menu_icon'  => 'dashicons-cart',
            'show_bar_menu' => false,
            ) );
        

            // SKU Controls
            CSF::createSection( $prefix, array(
            'title'  => 'SKU Controls',
            'fields' => array(

                array(
                    'id'    => 'skulabel_pro_sku_control',
                    'type'  => 'switcher',
                    'title' => 'Enable SKU',
                    'default' => true,
                  ),

                array(
                'id'    => 'skulabel_pro_sku_label',
                'type'  => 'text',
                'title' => 'Custom SKU Label',
                ),
                      
            )
            
            ) );


            // MPN Controls
            CSF::createSection( $prefix, array(
            'title'  => 'MPN Controls',
            'fields' => array(
        
                array(
                    'id'    => 'skulabel_pro_mpn_control',
                    'type'  => 'switcher',
                    'title' => 'Enable MPN',
                    'default' => false,
                  ),

                  array(
                    'id'    => 'skulabel_pro_mpn_frontend_display',
                    'type'  => 'switcher',
                    'title' => 'Disable Frontend View',
                    'subtitle' => 'It will allow you to hide the MPN from frontend. But, will avalilbe on WordPress admin.',
                    'default' => false,
                  ),

                array(
                'id'    => 'skulabel_pro_mpn_label',
                'type'  => 'text',
                'title' => 'Custom MPN Label',
                'default' => 'MPN',
                ),
        
            )
            ) );


            // UPC Controls
            CSF::createSection( $prefix, array(
                'title'  => 'UPC Controls',
                'fields' => array(
            
                    array(
                        'id'    => 'skulabel_pro_upc_control',
                        'type'  => 'switcher',
                        'title' => 'Enable UPC',
                        'default' => false,
                      ),

                      array(
                        'id'    => 'skulabel_pro_upc_frontend_display',
                        'type'  => 'switcher',
                        'title' => 'Disable Frontend View',
                        'subtitle' => 'It will allow you to hide the UPC ßfrom frontend. But, will avalilbe on WordPress admin.',
                        'default' => false,
                      ),
    
                    array(
                    'id'    => 'skulabel_pro_upc_label',
                    'type'  => 'text',
                    'title' => 'Custom UPC Label',
                    ),
            
                )
                ) );
        
        }
        