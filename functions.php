<?php

    // Require all files and libraries
    require_once plugin_dir_path( __FILE__ ) .'admin.php'; // Plugin Settings Panel
    require_once plugin_dir_path( __FILE__ ) .'sku-control.php'; // All SKU Controls
    require_once plugin_dir_path( __FILE__ ) .'mpn-control.php'; // All MPN Controls
    require_once plugin_dir_path( __FILE__ ) .'upc-control.php'; // All UPC Controls


    require 'inc/updater/plugin-update-checker.php';
    $pluginUpdater = Puc_v4_Factory::buildUpdateChecker('https://gitlab.com/anonnaabir/sku-label-changer-pro',__FILE__,'skumpn-label');