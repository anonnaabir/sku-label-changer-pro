<?php

/**
 *
 * @link              https://codember.com/
 * @since             1.1
 * @package           SKU Label Changer Pro
 *
 * @wordpress-plugin
 * Plugin Name:       SKU Label Changer Pro For WooCommerce
 * Plugin URI:        https://codember.com/
 * Description:       Change WooCommerce default SKU label, add custom MPN & UPC label or even add a new custom product code.
 * Version:           1.1
 * Author:            Codember
 * Author URI:        https://codember.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       skulabel-pro
 * Tested up to:      5.6

 * WC requires at least: 2.2
 * WC tested up to: 4.7.0

 */

    /*
        Here is the main function to change the SKU Label
    */

    if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
        require_once ('functions.php');
    }

    else {
        function skulabel_pro_woocomerce_error_notice() {
            ?>
            <div class="notice notice-error is-dismissible" style="background-color:#7F54B3;">
                <p 
                style="color:#FFFFFF;
                font-size:16px;
                ">
                <strong>SKU Label Changer Pro need an active installation of WooCommerce.</strong> Please install WooCommerce first.</p>
            </div>
            <?php
        }
        add_action( 'admin_notices', 'skulabel_pro_woocomerce_error_notice' );
    }