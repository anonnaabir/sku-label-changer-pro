<?php

        class CE_MPN {
            
            public function __construct() {
                $options = get_option( 'skulabel_pro_admin' );
                $mpn_control = $options['skulabel_pro_mpn_control'];
                $mpn_frontend_display = $options['skulabel_pro_mpn_frontend_display'];

                if ($mpn_control == true) {
                add_action( 'woocommerce_product_options_inventory_product_data', array($this,'skulabel_pro_add_mpn_field'));
                add_action( 'woocommerce_process_product_meta', array($this, 'skulabel_pro_save_mpn_data'));
                add_filter( 'manage_edit-product_columns', array($this,'skulabel_pro_mpn_admin_column'), 8888 );
                add_action( 'manage_product_posts_custom_column', array($this,'skulabel_pro_mpn_admin_column_content'), 10, 2 );
                add_filter( 'manage_edit-product_sortable_columns', array($this,'skulabel_pro_mpn_column_sortable') );
                    if ($mpn_frontend_display == false) {
                        add_action( 'woocommerce_product_meta_start', array($this,'skulabel_pro_mpn_display_data'));
                    }
                }

            }


            // Display MPN Field On Backend

            public function skulabel_pro_add_mpn_field() {

                $options = get_option( 'skulabel_pro_admin' );
                $mpn_label = $options['skulabel_pro_mpn_label'];

                if (empty($mpn_label)) {
                    $mpn_label = "MPN";
                }

                $args = array(
                    'id' => 'skulabel_pro_mpn_field',
                    'label' => __( $mpn_label, 'skumpn-label' ),
                    'class' => 'cfwc-custom-field',
                    'desc_tip' => true,
                    'description' => __( 'Enter the title of your custom text field.', 'skulabel-pro' ),
                    );
                    woocommerce_wp_text_input( $args );
            }


            // Save MPN Field Data

                public function skulabel_pro_save_mpn_data($post_id) {
                    $product = wc_get_product( $post_id );
                    $title = isset( $_POST['skulabel_pro_mpn_field'] ) ? $_POST['skulabel_pro_mpn_field'] : '';
                    $product->update_meta_data( 'skulabel_pro_mpn_field', sanitize_text_field( $title ) );
                    $product->save();
                }


            // Display MPN Field Data On Frontend

            public function skulabel_pro_mpn_display_data() {
                
                $options = get_option( 'skulabel_pro_admin' );
                $mpn_label = $options['skulabel_pro_mpn_label'];

                if (empty($mpn_label)) {
                    $mpn_label = "MPN";
                }

                global $post;
                // Check for the custom field value
                $product = wc_get_product( $post->ID );
                $data = $product->get_meta( 'skulabel_pro_mpn_field' );
                
                    if($data) {
                    // Only display our field if we've got a value for the field title
                    echo "<strong>";
                    echo $mpn_label.": ";
                    echo "</strong>";
                    echo $data."<br>";
                }
            }


                // Display Tab On WooCommere Product Admin Column


                public function skulabel_pro_mpn_admin_column( $columns ){
                    
                    $options = get_option( 'skulabel_pro_admin' );
                    $mpn_label = $options['skulabel_pro_mpn_label'];

                        if (empty($mpn_label)) {
                            $mpn_label = "MPN";
                        }

                    $columns['skulabel_pro_mpn_field'] = $mpn_label;
                    return $columns;
                }

                // Display Data On WooCommere Product Admin Column

                public function skulabel_pro_mpn_admin_column_content( $column, $product_id ){
                    if ( $column == 'skulabel_pro_mpn_field' ) {
                        $product = wc_get_product( $product_id );
                    echo $product->get_meta( 'skulabel_pro_mpn_field' );
                    }
                }

                
                // Make The MPN Column Shortable

                public function skulabel_pro_mpn_column_sortable( $columns ){

                    $options = get_option( 'skulabel_pro_admin' );
                    $mpn_label = $options['skulabel_pro_mpn_label'];

                        if (empty($mpn_label)) {
                            $mpn_label = "MPN";
                        }

                    $columns['skulabel_pro_mpn_field'] = $mpn_label;
                    return $columns;
                }

        }


        new CE_MPN();
        