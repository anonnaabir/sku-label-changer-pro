<?php


        class CE_SKU {

            // Get SKU Options

            public function __construct() {
                $options = get_option( 'skulabel_pro_admin' );
                $sku_control = $options['skulabel_pro_sku_control'];
                
                add_filter('gettext', array($this,'skulabel_pro_change_sku_label'), 20, 3);

                if ($sku_control == false) {
                    add_filter( 'wc_product_sku_enabled', '__return_false' );
                }

            }


            public function skulabel_pro_change_sku_label ($sku_label, $text, $domain) {

                $options = get_option( 'skulabel_pro_admin' );
                $new_sku_label = $options['skulabel_pro_sku_label'];

                if($domain = 'woocommerce') {
                switch ($sku_label) {
                    case 'SKU':
                        $sku_label = $new_sku_label;
                    break;
                    case 'SKU:':
                        $sku_label = $new_sku_label.":";
                    break;
                }
                    return $sku_label;
                }
            
            }

        }


        new CE_SKU();

        