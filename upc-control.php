<?php

        class CE_UPC {
            
            public function __construct() {
                $options = get_option( 'skulabel_pro_admin' );
                $upc_control = $options['skulabel_pro_upc_control'];
                $upc_frontend_display = $options['skulabel_pro_upc_frontend_display'];

                if ($upc_control == true) {
                    add_action( 'woocommerce_product_options_inventory_product_data', array($this,'skulabel_pro_add_upc_field'));
                    add_action( 'woocommerce_process_product_meta', array($this, 'skulabel_pro_save_upc_data'));
                    add_filter( 'manage_edit-product_columns', array($this,'skulabel_pro_upc_admin_column'), 9999 );
                    add_action( 'manage_product_posts_custom_column', array($this,'skulabel_pro_upc_admin_column_content'), 11, 2 );
                    add_filter( 'manage_edit-product_sortable_columns', array($this,'skulabel_pro_upc_column_sortable') );
                        if ($upc_frontend_display == false) {
                            add_action( 'woocommerce_product_meta_start', array($this,'skulabel_pro_upc_display_data'));
                        }
                }
                
            }


            // Display UPC Field On Backend

            public function skulabel_pro_add_upc_field() {

                $options = get_option( 'skulabel_pro_admin' );
                $upc_label = $options['skulabel_pro_upc_label'];

                if (empty($mpn_label)) {
                    $upc_label = "UPC";
                }

                $args = array(
                    'id' => 'skulabel_pro_upc_field',
                    'label' => __( $upc_label, 'skumpn-label' ),
                    'desc_tip' => true,
                    'description' => __( 'Enter the title of your custom text field.', 'ctwc' ),
                    );
                    woocommerce_wp_text_input( $args );
            }


            // Save UPC Field Data

            public function skulabel_pro_save_upc_data($post_id) {
                $product = wc_get_product( $post_id );
                $title = isset( $_POST['skulabel_pro_upc_field'] ) ? $_POST['skulabel_pro_upc_field'] : '';
                $product->update_meta_data( 'skulabel_pro_upc_field', sanitize_text_field( $title ) );
                $product->save();
            }


            // Display UPC Field Data On Frontend

            public function skulabel_pro_upc_display_data() {

                $options = get_option( 'skulabel_pro_admin' );
                $upc_label = $options['skulabel_pro_upc_label'];

                if (empty($mpn_label)) {
                    $upc_label = "UPC";
                }

                global $post;
                // Check for the custom field value
                $product = wc_get_product( $post->ID );
                $data = $product->get_meta( 'skulabel_pro_upc_field' );
                
                    if($data) {
                    // Only display our field if we've got a value for the field title
                    echo "<strong>";
                    echo $upc_label.": ";
                    echo "</strong>";
                    echo $data."<br>";
                }
            }


            // Display UPC Tab On WooCommere Product Admin Column


            public function skulabel_pro_upc_admin_column( $columns ){
                    
                $options = get_option( 'skulabel_pro_admin' );
                $upc_label = $options['skulabel_pro_upc_label'];

                if (empty($mpn_label)) {
                    $upc_label = "UPC";
                }

                $columns['skulabel_pro_mpn_field'] = $upc_label;
                return $columns;
            }

            // Display UPC Data On WooCommere Product Admin Column

            public function skulabel_pro_upc_admin_column_content( $column, $product_id ){
                if ( $column == 'skulabel_pro_upc_field' ) {
                    $product = wc_get_product( $product_id );
                echo $product->get_meta( 'skulabel_pro_upc_field' );
                }
            }

            
            // Make The UPC Column Shortable

            public function skulabel_pro_upc_column_sortable( $columns ){

                $options = get_option( 'skulabel_pro_admin' );
                $upc_label = $options['skulabel_pro_upc_label'];

                if (empty($mpn_label)) {
                    $upc_label = "UPC";
                }

                $columns['skulabel_pro_upc_field'] = $upc_label;
                return $columns;
            }

        }


        new CE_UPC();